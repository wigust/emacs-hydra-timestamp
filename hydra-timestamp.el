;;; hydra-timestamp.el --- Hydra functions to insert timestamps  -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Oleg Pykhalov

;; Author: Oleg Pykhalov <go.wigust@gmail.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Origin <https://www.wisdomandwonder.com/article/10763/emacsorg-mode-hydra-to-insert-timestamps>.
;; This package provides ‘hydra’ functions to insert timestamps.

;;; Code:

(defhydra hydra-timestamp (:color blue :hint nil)
  "
Timestamps: (_q_uit)
  Date: _I_SO, _U_S, US With _Y_ear and _D_ashes, US In _W_ords
   Date/Time: _N_o Colons or _w_ith
    Org-Mode: _R_ight Now or _c_hoose
"
  ("q" nil)

  ("I" hydra-timestamp-insert-datestamp)
  ("U" hydra-timestamp-insert-datestamp-us)
  ("Y" hydra-timestamp-insert-datestamp-us-full-year)
  ("D" hydra-timestamp-insert-datestamp-us-full-year-and-dashes)
  ("W" hydra-timestamp-insert-datestamp-us-words)

  ("N" hydra-timestamp-insert-timestamp-no-colons)
  ("w" hydra-timestamp-insert-timestamp)

  ("R" hydra-timestamp-org-time-stamp-with-seconds-now)
  ("c" org-time-stamp))

(defun hydra-timestamp-insert-datestamp ()
  "Produces and inserts a partial ISO 8601 format timestamp."
  (interactive)
  (insert (format-time-string "%F")))

(defun hydra-timestamp-insert-datestamp-us ()
  "Produces and inserts a US datestamp."
  (interactive)
  (insert (format-time-string "%m/%d/%y")))

(defun hydra-timestamp-insert-datestamp-us-full-year-and-dashes ()
  "Produces and inserts a US datestamp with full year and dashes."
  (interactive)
  (insert (format-time-string "%m-%d-%Y")))

(defun hydra-timestamp-insert-datestamp-us-full-year ()
  "Produces and inserts a US datestamp with full year."
  (interactive)
  (insert (format-time-string "%m/%d/%Y")))

(defun hydra-timestamp-insert-datestamp-us-words ()
  "Produces and inserts a US datestamp using words."
  (interactive)
  (insert (format-time-string "%A %B %d, %Y")))

(defun hydra-timestamp-insert-timestamp-no-colons ()
  "Inserts a full ISO 8601 format timestamp with colons replaced by hyphens."
  (interactive)
  (insert (hydra-timestamp-get-timestamp-no-colons)))

(defun hydra-timestamp-insert-datestamp ()
  "Produces and inserts a partial ISO 8601 format timestamp."
  (interactive)
  (insert (format-time-string "%F")))

(defun hydra-timestamp-get-timestamp-no-colons ()
  "Produces a full ISO 8601 format timestamp with colons replaced by hyphens."
  (interactive)
  (let* ((timestamp (hydra-timestamp-get-timestamp))
         (timestamp-no-colons (replace-regexp-in-string ":" "-" timestamp)))
    timestamp-no-colons))

(defun hydra-timestamp-get-timestamp ()
  "Produces a full ISO 8601 format timestamp."
  (interactive)
  (let* ((timestamp-without-timezone (format-time-string "%Y-%m-%dT%T"))
         (timezone-name-in-numeric-form (format-time-string "%z"))
         (timezone-utf-offset
          (concat (substring timezone-name-in-numeric-form 0 3)
                  ":"
                  (substring timezone-name-in-numeric-form 3 5)))
         (timestamp (concat timestamp-without-timezone
                            timezone-utf-offset)))
    timestamp))

(defun hydra-timestamp-insert-timestamp ()
  "Inserts a full ISO 8601 format timestamp."
  (interactive)
  (insert (hydra-timestamp-get-timestamp)))

(defun hydra-timestamp-org-time-stamp-with-seconds-now ()
  (interactive)
  (let ((current-prefix-arg '(16)))
    (call-interactively 'org-time-stamp)))

(provide 'hydra-timestamp)
;;; hydra-timestamp.el ends here
